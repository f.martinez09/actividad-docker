package c.lufro.dci.supermercadoapi.models;

public class UsuarioReponedor extends Usuario{
    private String seccion;

    public UsuarioReponedor(String nombre, int rut, String contraseña, String seccion) {
        this.nombre = nombre;
        this.rut = rut;
        this.contraseña = contraseña;
        this.seccion = seccion;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }
}
