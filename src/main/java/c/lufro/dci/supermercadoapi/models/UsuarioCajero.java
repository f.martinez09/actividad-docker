package c.lufro.dci.supermercadoapi.models;

public class UsuarioCajero extends Usuario{
    private int ventas;

    public UsuarioCajero(String nombre, int rut, String contraseña, int ventas) {
        this.nombre = nombre;
        this.rut = rut;
        this.contraseña = contraseña;
        this.ventas = ventas;
    }

    public int getVentas() {
        return ventas;
    }

    public void setVentas(int ventas) {
        this.ventas = ventas;
    }
}
