package c.lufro.dci.supermercadoapi.controllers;

import c.lufro.dci.supermercadoapi.models.Merma;
import c.lufro.dci.supermercadoapi.models.Producto;
import c.lufro.dci.supermercadoapi.models.UsuarioCajero;
import c.lufro.dci.supermercadoapi.models.UsuarioReponedor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("Api")
public class ApiController {

    private static List<Producto> listaProductos = new ArrayList<>();
    private static List<Merma> listaMermas = new ArrayList<>();
    private static List<UsuarioReponedor> ListaReponedores = new ArrayList<>();
    private static List<UsuarioCajero> ListaCajeros = new ArrayList<>();

    @GetMapping("/productos")
    public List<Producto> getProductos(){
        listaProductos.add(new Producto("Pan de Molde", "Panadería", 10, 2400, "Panadería"));
        listaProductos.add(new Producto("Queso gauda", "Panadería", 10, 2400, "Panadería"));
        listaProductos.add(new Producto("Pan de Molde", "Panadería", 10, 2400, "Panadería"));
        listaProductos.add(new Producto("Pan de Molde", "Panadería", 10, 2400, "Panadería"));
        listaProductos.add(new Producto("Pan de Molde", "Panadería", 10, 2400, "Panadería"));
        listaProductos.add(new Producto("Pan de Molde", "Panadería", 10, 2400, "Panadería"));
        listaProductos.add(new Producto("Pan de Molde", "Panadería", 10, 2400, "Panadería"));
        listaProductos.add(new Producto("Pan de Molde", "Panadería", 10, 2400, "Panadería"));
        return listaProductos;
    }

    @GetMapping("/reponedor")
    public List<UsuarioReponedor> getReponedores() {
        ListaReponedores.add(new UsuarioReponedor("Javier", 123123123,"asdasd", "Marketing"));
        ListaReponedores.add(new UsuarioReponedor("Javier", 123123123,"asdasd", "Marketing"));
        ListaReponedores.add(new UsuarioReponedor("Javier", 123123123,"asdasd", "Marketing"));
        return ListaReponedores;
    }

    @GetMapping("/cajero")
    public List<UsuarioCajero> getCajeros() {
        ListaCajeros.add(new UsuarioCajero("Camilo", 12345678, "asdasd123", 4));
        ListaCajeros.add(new UsuarioCajero("Camilo", 12345678, "asdasd123", 4));
        ListaCajeros.add(new UsuarioCajero("Camilo", 12345678, "asdasd123", 4));
        ListaCajeros.add(new UsuarioCajero("Camilo", 12345678, "asdasd123", 4));
        ListaCajeros.add(new UsuarioCajero("Camilo", 12345678, "asdasd123", 4));
        return ListaCajeros;
    }

    @GetMapping("/merma")
    public List<Merma> getMermas() {
        listaMermas.add(new Merma("Pan de Molde", "Motivo", 100));
        listaMermas.add(new Merma("Pan de Molde", "Motivo", 100));
        listaMermas.add(new Merma("Pan de Molde", "Motivo", 100));
        return listaMermas;
    }
}
