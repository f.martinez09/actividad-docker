FROM openjdk:11

VOLUME /tmp

EXPOSE 8080

COPY target/supermercado-api-0.0.1-SNAPSHOT.jar supermercado-api.jar

ENTRYPOINT ["java", "-jar", "supermercado-api.jar"]

#sudo docker build -t supermercado-api .
#sudo docker run -p 8080:8080 -d supermercado-api